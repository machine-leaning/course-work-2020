# !/usr/bin/env python3

from pandas import Series
from pandas import merge
from numpy import float64
from numpy.random import seed
from IPython import get_ipython
from os import makedirs
from pandas import merge
from requests import get
from os import listdir
from os.path import isfile
from pandas import read_csv
from pandas import concat
from numpy import array
from re import compile
from re import findall
from pathlib import Path
from zipfile import ZipFile
from zipfile import is_zipfile
from datetime import datetime
from os.path import exists
from sklearn.preprocessing import QuantileTransformer
from time import mktime
from os.path import join
from pandas import DataFrame
from sys import stdout
from scipy import stats
from numpy import abs, nan
from numpy import where
from tqdm import tqdm, trange
from matplotlib.pyplot import figure
from matplotlib.pyplot import scatter
from matplotlib.pyplot import rcParams
from matplotlib.pyplot import xlabel
from matplotlib.pyplot import ylabel
from matplotlib.pyplot import title
from matplotlib.pyplot import show
from numpy import diagonal
from scipy.spatial.distance import pdist, squareform
from sklearn.metrics import silhouette_samples, silhouette_score
from sklearn.cluster import KMeans
from pandas import DataFrame
from matplotlib.pyplot import scatter
from matplotlib.pyplot import figure
from matplotlib.pyplot import show
from matplotlib.pyplot import xlabel
from matplotlib.pyplot import ylabel
from kneed import KneeLocator


class OutlierDetectionWithML(object):
    """

    """

    __INDEX = 'index'
    __SILHOUETTE_VALUES_FOR__ = 'sample_silhouette_values'
    __FINALIZED_DATA = 'finalized_data'

    def __init__(self, metric='euclidean', number_of_max_clusters_to_check=10, workspace_variables=globals(),
                 figure_counter=1):
        self.__workspace_variables = workspace_variables
        self.__figure_counter = figure_counter
        self.__machine_learning_algorithm(metric, number_of_max_clusters_to_check)

    def get_figure_counter(self):
        """

        :return:

        """
        return self.__figure_counter

    def __machine_learning_algorithm(self, metric, number_of_max_clusters_to_check=10):
        """

        :param number_of_max_clusters_to_check:
        :return:

        """

        if number_of_max_clusters_to_check < 3:
            raise Exception('Wrong number of clusters: {} specified'.format(number_of_max_clusters_to_check))

        figure(self.__figure_counter)
        self.__figure_counter = self.__figure_counter + 1
        if self.__SILHOUETTE_VALUES_FOR__ not in self.__workspace_variables:
            self.__workspace_variables[self.__SILHOUETTE_VALUES_FOR__] = {}
        for n_clusters in range(3, number_of_max_clusters_to_check):
            print(f'start training {n_clusters}')
            clusterer = KMeans(n_clusters=n_clusters, random_state=10)
            print(f'end training {n_clusters}')
            print(f'start fit {n_clusters}')
            cluster_labels = clusterer.fit_predict(self.__workspace_variables[self.__FINALIZED_DATA])
            print(f'end fit {n_clusters}')

            # Silluette score gives the average value for all the samples.
            # This gives a perspective into the density and separation of the formed
            # clusters
            print(f'Start calculating silhouette_score ...')
            silhuette_avg = silhouette_score(self.__workspace_variables[self.__FINALIZED_DATA], cluster_labels)
            print(f'End Calculation silhouette_score. For n_clusters {n_clusters}, the average silhuette_score'
                  f' is {silhuette_avg}')

            # Compute Shilluette score for each sample
            self.__workspace_variables[self.__SILHOUETTE_VALUES_FOR__][n_clusters] = \
                silhouette_samples(self.__workspace_variables[self.__FINALIZED_DATA], cluster_labels)
        self.__plot_distance(metric)
        show()

    def __plot_distance(self, metric='euclidean'):
        """

        https://raghavan.usc.edu/papers/kneedle-simplex11.pdf

        :param metric:
        :return:

        """
        figure(self.__figure_counter)
        self.__figure_counter = self.__figure_counter + 1
        columns = [column for column, types in self.__workspace_variables[self.__FINALIZED_DATA].dtypes.iteritems()]
        data_frame_values = self.__workspace_variables[self.__FINALIZED_DATA].copy(deep=True).sort_values(columns)\
            .reset_index(drop=True).values
        dist_data_frame = DataFrame(data=squareform(pdist(data_frame_values, metric)), columns=columns)
        x = dist_data_frame.index
        y = diagonal(dist_data_frame.values, offset=1).sort_values(columns).reset_index(drop=True)
        scatter(x, y)
        xlabel('Eucledeian distances')
        ylabel('number of clusters k')

        figure(self.__figure_counter)
        self.__figure_counter = self.__figure_counter + 1

        df = DataFrame(data=[y], columns=['seq_row_distance'])
        df2 = df[df['seq_row_distance'] < 0.5]
        df2 = df2.sort_values(by=['seq_row_distance']).reset_index(drop=True)
        print('considering min distance between 0,1 to 0.5')
        scatter(df2['index'], df2['seq_row_distance'])
        print('considered min distance between 0,1 to 0.5')

        figure(self.__figure_counter)
        self.__figure_counter = self.__figure_counter + 1
        print('finding Knee')
        kn = KneeLocator(df2['index'], df2['seq_row_distance'], curve='convex', direction='increasing')
        print('knee found')
        scatter(kn.knee, 'bx-')


class GIGEncoder:
    """

    NormalInverseGammaEncoder

    https://arxiv.org/pdf/1904.13001.pdf

    GaussianInverseGammaEncoder is used to encode categorical features with a Normal - GIG
    conjugate pair model (i.e. a Normal Inverse Gamma posterior distribution for a normally
    distributed target).  https://en.wikipedia.org/wiki/Normal-inverse-gamma_distribution

    For each categorical feature, this object stores a updated parameters mu, v, alpha, beta
    columns with a row for each existing level of the categorical feature.

    interpretation:
    mean was estimated from {\displaystyle \nu } \nu
    observations with sample mean {\displaystyle \mu _{0}} \mu _{0};
    variance was estimated from {\displaystyle 2\alpha } 2\alpha
    observations with sample mean {\displaystyle \mu _{0}} \mu _{0}
    and sum of squared deviations {\displaystyle 2\beta } 2\beta

    The input to fit() should be an array-like of real numbers for y
    and array-like of strings for X.
    The output of transform() will be <column>__[M]_[X | V] where [M] is a particular moment
    of the normal inverse gamma distribution [‘mvsk’] (m and v are default) and X or sigma refers to
    expected value of X or Variance.
    By default, a prior of u_0=0, v=1, alpha =2, beta=1  (Standard Gaussian) is used.

    Parameters
    ----------
    u_0 (float): prior. default = 0
    v (float): prior. default = 1
    beta (float): prior for beta. default = 1
    alpha (float):  prior for alpha. default = 2
    random_state (integer): random state for bootstrap samples. default = 1
    n_samples (integer): number of bootstrap samples. default = 100

    Attributes
    ----------
    _u_0_prior (float): prior. default = 0
    _v_prior (float): prior. default = 1
    _beta_prior (float) - prior for beta. default = .5
    _alpha_prior (float) - prior for alpha. default = .5
    _random_state (integer): random state for bootstrap samples. default = 1
    _n_samples (integer): number of bootstrap samples. default = 100
    _gig_distributions (dict) - houses the categorical beta distributions
        in pandas dataframes with cols `alpha` and `beta`


    Methods
    ----------
    fit()
    transform()


    Examples
    --------
    import pandas as pd
    import pymc3 as pm
    from normal_inverse_gamma_encoder import GIGEncoder

    data = read_csv(pm.get_data('radon.csv'))
    data = data[['county', 'log_radon', 'floor']]
    enc = GIGEncoder()
    enc.fit(data[['county', 'floor']],data['log_radon'],['county', 'floor'])
    enc._gig_distributions
    enc.transform(data[['county', 'floor']],columns=['county', 'floor']).head()

    """

    def __init__(self, u_0=0.0, v=1.0, alpha=3.0, beta=1.0, n_samples=10, sample_size=.75, random_state=1):
        """

        init for BetaEncoder
        Args:
            alpha - prior for number of successes
            beta - prior for number of failures

        """
        # Validate Types
        if type(u_0) is not float and type(u_0) is not float64:
            raise AttributeError("Argument 'u_0' must be of type float")
        if type(v) is not float and type(v) is not float64:
            raise AttributeError("Argument 'v' must be of type float")
        if type(alpha) != float:
            raise AttributeError("Argument 'alpha' must be of type float")
        if type(beta) is not float:
            raise AttributeError("Argument 'beta' must be of type float")
        if type(n_samples) is not int:
            raise AttributeError("Argument 'n_samples' must be of type int")
        if type(random_state) is not int:
            raise AttributeError("Argument 'random_state' must be of type int")

        # Assign
        self._alpha_prior = alpha
        self._beta_prior = beta
        self._u_0_prior = u_0
        self._v_prior = v
        self._gig_distributions = dict()
        self._random_state = random_state
        self._n_samples = n_samples
        self._sample_size = sample_size
        seed(random_state)

    def fit(self, x, y, columns=None):
        """

        fit
        Method to fit self.beta_distributions
        from x and y

        Args:
            x (array-like) - categorical columns
            y (array-like) - target column (float))
            columns (list of str) - list of column names to fit
                otherwise, attempt to fit just string columns
        Returns:
            beta_distributions (dict) - a dict of pandas DataFrame for each
                categorical column with beta and alpha for each level
        """
        if len(x) != len(y):
            print("received: ", len(x), len(y))
            raise AssertionError("Length of X and y must be equal.")

        x_temp = x.copy(deep=True)
        categorical_cols = columns
        if not categorical_cols:
            categorical_cols = self.get_string_cols(x_temp)

        # add target
        target_col = '_target'
        x_temp[target_col] = y

        for categorical_col in categorical_cols:

            # All Levels
            #   Bootstrap samples may not contain all levels, so fill NA with priors
            all_levels = x_temp[[categorical_col, target_col]].groupby(categorical_col).count().reset_index()

            for i in range(self._n_samples):

                x_sample = x_temp[[categorical_col, target_col]].sample(n=int(len(x_temp) * self._sample_size),
                                                                        replace=True,
                                                                        random_state=self._random_state + i)

                # full count (n)
                full_count = x_sample[[categorical_col, target_col]].groupby(categorical_col).count().reset_index()
                full_count = full_count.rename(index=str, columns={target_col: categorical_col + "_full_count"})

                # mean
                sample_mean = x_sample[[categorical_col, target_col]].groupby(categorical_col).mean().reset_index()
                sample_mean = sample_mean.rename(index=str, columns={target_col: categorical_col + "_sample_mean"})

                # std
                std = x_sample[[categorical_col, target_col]].groupby(categorical_col).std().reset_index()
                std = std.rename(index=str, columns={target_col: categorical_col + "_std"})

                # merge them
                temp = merge(full_count, sample_mean, on=[categorical_col])
                temp = merge(temp, std, on=[categorical_col])

                # weighted means
                temp['_u'] = ((self._v_prior * self._u_0_prior) + (
                        temp[categorical_col + "_full_count"] * temp[categorical_col + "_sample_mean"])) / \
                             (self._v_prior + temp[categorical_col + "_full_count"])

                # new count
                temp['_v'] = self._v_prior + temp[categorical_col + "_full_count"]

                # new alpha
                temp['_alpha'] = self._alpha_prior + (temp[categorical_col + "_full_count"] / 2)

                # new beta
                temp['_beta'] = self._beta_prior + (.5 * (temp[categorical_col + "_std"] ** 2)) + \
                                ((temp[categorical_col + "_full_count"] * self._v_prior) /
                                 (temp[categorical_col + "_full_count"] + self._v_prior)) * \
                                (((temp[categorical_col + "_sample_mean"] - self._u_0_prior) ** 2) / 2)

                # fill NAs with prior
                temp = merge(all_levels, temp, on=categorical_col, how='left')
                temp['_u'] = temp['_u'].fillna(self._u_0_prior)
                temp['_v'] = temp['_v'].fillna(self._v_prior)
                temp['_alpha'] = temp['_alpha'].fillna(self._alpha_prior)
                temp['_beta'] = temp['_beta'].fillna(self._beta_prior)

                if categorical_col not in self._gig_distributions.keys():
                    self._gig_distributions[categorical_col] = temp[[categorical_col, '_u', '_v', '_alpha', '_beta']]
                else:
                    self._gig_distributions[categorical_col][['_u', '_v', '_alpha', '_beta']] += temp[
                        ['_u', '_v', '_alpha', '_beta']]

            # report mean alpha and beta:
            self._gig_distributions[categorical_col]['_u'] = self._gig_distributions[categorical_col][
                                                                 '_u'] / self._n_samples
            self._gig_distributions[categorical_col]['_v'] = self._gig_distributions[categorical_col][
                                                                 '_v'] / self._n_samples
            self._gig_distributions[categorical_col]['_alpha'] = self._gig_distributions[categorical_col][
                                                                     '_alpha'] / self._n_samples
            self._gig_distributions[categorical_col]['_beta'] = self._gig_distributions[categorical_col][
                                                                    '_beta'] / self._n_samples
        return

    def transform(self, x, moments='m', columns=None):
        """
        transform
        Args:
            x (array-like) - categorical columns matching
                the columns in beta_distributions
            columns (list of str) - list of column names to transform
                otherwise, attempt to transform just string columns
            moments (str) - composed of letters [‘mvsk’]
                specifying which moments to compute where ‘m’ = mean,
                ‘v’ = variance, ‘s’ = (Fisher’s) skew and ‘k’ = (Fisher’s)
                kurtosis. (default=’m’)
        """
        x_temp = x.copy(deep=True)
        categorical_cols = columns
        if not categorical_cols:
            categorical_cols = self.get_string_cols(x_temp)

        for categorical_col in categorical_cols:
            if categorical_col not in self._gig_distributions.keys():
                raise AssertionError("Column " + categorical_col + " not fit by GIGEncoder")

            # add `_alpha` and `_beta` columns vi lookups, impute with prior
            x_temp = x_temp.merge(self._gig_distributions[categorical_col], on=[categorical_col], how='left')

            x_temp['_u'] = x_temp['_u'].fillna(self._u_0_prior)
            x_temp['_v'] = x_temp['_v'].fillna(self._v_prior)
            x_temp['_alpha'] = x_temp['_alpha'].fillna(self._alpha_prior)
            x_temp['_beta'] = x_temp['_beta'].fillna(self._beta_prior)

            #   encode with moments
            if 'm' in moments:
                x_temp[categorical_col + '__M_u'] = x_temp["_u"]

                # check alpha > 1
                if (x_temp['_alpha'] <= 1).any():
                    raise ValueError("'alpha' must be greater than 1")
                x_temp[categorical_col + '__M_v'] = x_temp["_beta"] / (x_temp['_alpha'] - 1)

            if 'v' in moments:

                x_temp[categorical_col + '__V_u'] = x_temp["_beta"] / ((x_temp['_alpha'] - 1) * x_temp['_v'])

                if (x_temp['_alpha'] <= 2).any():
                    raise ValueError("'alpha' must be greater than 2")
                x_temp[categorical_col + '__V_v'] = (x_temp["_beta"] ** 2) / \
                                                    (((x_temp['_alpha'] - 1) ** 2) * (x_temp['_alpha'] - 2))

            # and drop columns
            x_temp = x_temp.drop([categorical_col], axis=1)
            x_temp = x_temp.drop(["_u"], axis=1)
            x_temp = x_temp.drop(["_v"], axis=1)
            x_temp = x_temp.drop(["_alpha"], axis=1)
            x_temp = x_temp.drop(["_beta"], axis=1)

        return x_temp

    @staticmethod
    def get_string_cols(df):
        idx = (df.applymap(type) == str).all(0)
        return df.columns[idx]


class OutliersZScoreVisual(object):
    """

    """

    __ALL_KEYS = 'all_keys'
    __ENCODED = '{}_encoded'
    __ENCODED_WITHOUT_OUTLIERS = '{}_encoded_without_z_outliers'
    __VALUE_COUNTS_OF = 'value_counts_of_{}'
    __Z_OUTLIERS = '{}_z_outliers'
    __INDEX = 'index'

    def __init__(self, plot_variables, workspace_variables=globals(), figure_counter=1):
        """

        :param workspace_variables:
        :param figure_counter:

        """
        self.__figure_counter = 1
        self.__plot_variables = plot_variables
        self.__workspace_variables = workspace_variables
        self.__figure_counter = figure_counter

    def get_figure_counter(self):
        """

        :return:

        """
        return self.__figure_counter

    def z_score_encoded(self, df_names):
        """

        """
        rcParams.update({'figure.max_open_warning': 0})
        for df_name_index in trange(len(df_names), file=stdout, leave=False, unit_scale=True,
                                    desc='Finding Outliers using z-score and cleaning data sets from them ...'):

            df_name = df_names[df_name_index]
            if self.__ENCODED_WITHOUT_OUTLIERS.format(df_name) not in self.__workspace_variables:
                self.__workspace_variables[self.__ENCODED_WITHOUT_OUTLIERS.format(df_name)] = {}
            if self.__VALUE_COUNTS_OF.format(self.__ENCODED.format(df_name)) not in self.__workspace_variables:
                self.__workspace_variables[self.__VALUE_COUNTS_OF.format(self.__ENCODED.format(df_name))] = {}
            if self.__Z_OUTLIERS.format(self.__ENCODED.format(df_name)) not in self.__workspace_variables:
                self.__workspace_variables[self.__Z_OUTLIERS.format(self.__ENCODED.format(df_name))] = {}
            dframe = self.__workspace_variables[self.__ENCODED.format(df_name)].copy(deep=True)
            types = dframe.dtypes
            types_keys = list(types.keys())
            for column_name_index in trange(len(types_keys), file=stdout, leave=False, unit_scale=True,
                                            desc='Finding Outliers using z-score and cleaning data set {} from them ...'
                                                    .format(df_name)):

                column_name = types_keys[column_name_index]
                if df_name in self.__plot_variables.keys() and column_name in self.__plot_variables[df_name]:
                    self.__workspace_variables[self.__VALUE_COUNTS_OF.format(self.__ENCODED.format(df_name))] \
                        [column_name] = dframe[column_name].value_counts()
                    df = self.__workspace_variables[self.__VALUE_COUNTS_OF.format(self.__ENCODED.format(df_name))] \
                        [column_name]
                    self.__workspace_variables[self.__Z_OUTLIERS.format(self.__ENCODED.format(df_name))][column_name], \
                    self.__workspace_variables[self.__ENCODED_WITHOUT_OUTLIERS.format(df_name)][column_name] = \
                        self.z_score_table_column(df.index.tolist(), df.tolist(), self.__ENCODED.format(df_name),
                                                  column_name, dframe)
                elif column_name != self.__INDEX:
                    self.__workspace_variables[self.__ENCODED_WITHOUT_OUTLIERS.format(df_name)][column_name] = \
                        dframe[column_name]

    def z_score_table_column(self, x_axis_data, table_column_data, table_name, column_name, table_df):
        """

        :param x_axis_data:
        :param table_column_data:
        :param table_name:
        :param column_name:
        :param table_df:
        :return:

        """

        print(f'start probability of occurrence {self.__figure_counter} - {table_name} - {column_name}')
        figure(self.__figure_counter)
        self.__figure_counter = self.__figure_counter + 1
        scatter(x_axis_data, table_column_data, color="maroon")
        xlabel(column_name, labelpad=15)
        ylabel(f"probability of occurrence \n - {table_name} - {column_name}", labelpad=14)
        title("{} {}".format(column_name, table_name), y=1.015, fontsize=20)
        print(f'start probability of occurrence {self.__figure_counter} - {table_name} - {column_name}')

        print(f'start z-score calculation and outlier removal {self.__figure_counter} - {table_name} - {column_name}')
        z = abs(stats.zscore(table_df[column_name]))
        z_outliers = where(z > 3)[0].tolist()
        dict_values_columns_to_replace = {}
        col_data = table_df[column_name]
        value_counts_col = self.__workspace_variables[self.__VALUE_COUNTS_OF.format(table_name)][column_name]
        avg_frequency = value_counts_col.sum() // len(value_counts_col.index)
        index_avg_value = value_counts_col.min()
        for index, freq in value_counts_col.iteritems():
            if freq < avg_frequency:
                index_avg_value = index
                break

        for missing_value_index in z_outliers:
            dict_values_columns_to_replace[col_data.iloc[missing_value_index]] = index_avg_value
        df_no_outliers = col_data.apply(
            lambda x: self.__func_dict(x, dict_values_columns_to_replace=dict_values_columns_to_replace)
        )
        print(f'end z-score calculation and outlier removal {self.__figure_counter} - {table_name} - {column_name}')

        print(f'start std {self.__figure_counter} - {table_name} - {column_name}')
        figure(self.__figure_counter)
        self.__figure_counter = self.__figure_counter + 1
        scatter(df_no_outliers.value_counts().index.tolist(), df_no_outliers.value_counts().tolist(), color="maroon")
        title(f"Standard Normal Distribution with \n no outliers", y=1.015, fontsize=20)
        xlabel(column_name, labelpad=15)
        ylabel(f"probability of occurrence \n - {table_name} - {column_name}", labelpad=14)
        print(f'end std {self.__figure_counter} - {table_name} - {column_name}')
        show()

        return z_outliers, df_no_outliers

    def __func_dict(self, x, dict_values_columns_to_replace):
        """

        :param x:
        :param dict_values_columns_to_replace:
        :return:

        """
        if x in dict_values_columns_to_replace.keys():
            return dict_values_columns_to_replace[x]
        else:
            return x


def get_all_unique_values(data_frame_names, pattern_data_set, pattern_unique, pattern_value_counts,
                          workspace_variables=globals()):
    """

    Gets the unique values for each column of each data frame. For each data frame we store a variable with name of
    pattern ^unique_values_of_[a-zA-Z][a-zA-Z0-9]*$
    Within each variable a dictionary is stored, where each key represents the name of each respective column and maps
    to the :class:`~ndarray` of the unique values

    :param workspace_variables: workspace_variables: the variables where all data are stored (defaults to global
            variables)
    :param data_frame_names: list of strings containing the data frame names
    :param pattern_data_set
    :param pattern_unique
    :param pattern_value_counts
    :return: a set of variables within workspace_variables, each of which have a structure as described above

    """

    print(f'\n')
    print(f'Loading unique values for all columns of each data frame of the data frames '
          f'{data_frame_names}!')

    for data_frame_name in trange(len(data_frame_names), file=stdout, leave=False, unit_scale=True,
                                  desc='finding unique values for all data set columns ...'):

        data_frame_name_value = data_frame_names[data_frame_name]
        types = workspace_variables[pattern_data_set.format(data_frame_name_value)].dtypes
        types_keys = list(types.keys())
        for column_name in trange(len(types_keys), file=stdout, leave=False, unit_scale=True,
                                  desc='finding unique values for all columns of data set {}...'
                                          .format(data_frame_name_value)):

            column_name_value = types_keys[column_name]
            if pattern_unique.format(pattern_data_set.format(data_frame_name_value)) not in workspace_variables:
                workspace_variables[pattern_unique.format(pattern_data_set.format(data_frame_name_value))] = {}
            if pattern_value_counts.format(pattern_data_set.format(data_frame_name_value)) not in workspace_variables:
                workspace_variables[pattern_value_counts.format(pattern_data_set.format(data_frame_name_value))] = {}
            workspace_variables[pattern_unique.format(pattern_data_set.format(data_frame_name_value))]\
                [column_name_value] = \
                    workspace_variables[pattern_data_set.format(data_frame_name_value)][column_name_value].unique()
            workspace_variables[pattern_value_counts.format(pattern_data_set.format(data_frame_name_value))]\
                [column_name_value] = \
                workspace_variables[pattern_data_set.format(data_frame_name_value)][column_name_value].value_counts()

    print(f'\n')
    print(f'Successfully brought unique values for all columns of each data frame of the data frames '
          f'{data_frame_names}!')


def get_descriptive_statistics(data_frame_names, workspace_variables=globals()):
    """

    :param workspace_variables: the variables where all data are stored (defaults to global variables)
    :param data_frame_names: list of strings containing the data frame names

    """

    print(f'\n')
    print(f'Loading descriptive statistics for all columns of each data frame of the data frames '
          f'{data_frame_names}!')

    for data_frame_name in trange(len(data_frame_names), file=stdout, leave=False, unit_scale=True,
                                  desc='finding descriptive statistics ...'):
        data_frame_name_value = data_frame_names[data_frame_name]
        workspace_variables['{}_descriptive_statistics'.format(data_frame_name_value)] = \
            workspace_variables[data_frame_name_value].describe()

    print(f'\n')
    print(f'Successfully calculated descriptive statistics for all columns of each data frame of the data frames '
          f'{data_frame_names}!')


def get_missing_values_if_pattern_not_match(data_frame_name, pattern, workspace_variables=globals()):
    """

    Gets all the missing values that do not match the applied pattern for each value of the given column name

    :param workspace_variables: the variables where all data are stored (defaults to global variables)
    :param data_frame_name: the data frame name string
    :param pattern: saves all the missing values that do not match the applied pattern for each value of the given
            column name

    """

    types = workspace_variables[data_frame_name].dtypes
    types_keys = types.keys()
    for column_name in types_keys:
        temp = workspace_variables[data_frame_name][workspace_variables[data_frame_name][column_name].apply(
            check_not_pattern_function, pattern=pattern)]
        workspace_variables['missing_values_{}'.format(data_frame_name)][column_name] = temp


def check_not_pattern_function(value, pattern):
    """

    Specific check to this data set only

    Compiles the pattern and searches if given value is not matched

    :param value: value to be checked if matched to given pattern
    :param pattern: pattern we check
    :return: True if given value is not matched to given pattern

    """
    pattern = compile(pattern)
    return not pattern.search(value)


def check_if_pointers_are_real(data_frame_name_from, column_name_from, data_frame_name_to, column_name_to,
                               workspace_variables=globals()):
    """

    Checks if there is a reference from left data frame to the right data frame

    :param workspace_variables: the variables where all data are stored (defaults to global variables)
    :param data_frame_name_from: left side data frame of relationship between the two data frames
    :param column_name_from: the name of the column of left side data frame of relationship between the two data frames
    :param data_frame_name_to: right side data frame of relationship between the two data frames
    :param column_name_to: he name of the column of right side data frame of relationship between the two data frames
    :return: True if there is a reference from left data frame to the right data frame

    """
    return workspace_variables[data_frame_name_from][column_name_from] == workspace_variables[data_frame_name_to][
        column_name_to]


def is_num(n):
    """

    Checks if n is a number

    :param n: the object being checked
    :return: True if n is a number

    """
    return isinstance(n, int) or isinstance(n, complex) or isinstance(n, float)


def has_str_only_num(value, total_types):
    """

    Checks if value is of string type and the pattern matches white-character-separated numbers

    :param value: the string value being checked
    :param total_types: a variable stored in the workspace in order to check that we have the minimal possible types
                        within data (acts as a proof of concept)
    :return: True if value is of string type and the pattern matches white-character-separated numbers

    """
    total_types.add(type(value))
    return isinstance(value, str) and len(findall('^[\d\t \n\r]+$', value)) > 0


def is_empty_str_or_none(value, col_type, total_types):
    """

    Checks if value string is none or numpy.nan or within the list of values ["", "unknown", "nan", "none"]

    ## TESTING: (df = devices data frame)


    # df = df.append({'user_id':'111', 'brand': 'Apple'}, ignore_index=True)
    # df = df.append({'user_id': None, 'brand': 'Samsung'}, ignore_index=True)
    # df = df.append({'user_id': '         ', 'brand': 'Sony'}, ignore_index=True)
    # df = df.append({'user_id': '', 'brand': 'Sony'}, ignore_index=True)
    # df = df.append({'user_id': np.nan, 'brand': 'Sony'}, ignore_index=True)

    # print( is_empty_str_or_none('Unknown') )
    # print( is_empty_str_or_none('   unknown') )
    # print( is_empty_str_or_none('UNKNOWN  ') )
    # print( is_empty_str_or_none('\n') )
    # print( is_empty_str_or_none('\n\r') )
    # print( is_empty_str_or_none('\t') )
    # print( is_empty_str_or_none('') )
    # print( is_empty_str_or_none('    ') )
    # print( is_empty_str_or_none(None) )

    :param value: the value to be checked
    :param col_type: the type of the value being checked
    :param total_types: a variable stored in the workspace in order to check that we have the minimal possible types
                        within data (acts as a proof of concept)
    :return: True if value string is none or math.nan or within the list of values ["", "unknown", "nan", "none"]

    # Old implementation:
    if value is None:
        return True
    if value.strip() == "":
        return True
    if value.lower().strip() == "unknown":
        return True
    return False
    """
    total_types.add(type(value))
    return ('object' != col_type and str(value).lower() in ["nan", "none"]) \
           or ('object' == col_type and value.lower().strip() in ["", "unknown", "nan", "none"])


def check_pattern_of_date_column(value, column_name, pattern):
    return 'date' in column_name.lower() and isinstance(value, str) and not datetime.strptime(value, pattern)


def get_missing_values(data_frame_names, ref_keys, workspace_variables=globals()):
    """

    Stores as many variables as the number of data frames is each of which contains a dictionary where the key is
    a column name of the respective data frame and the mapped value is an array of missing / wrong values

    :param data_frame_names: list of strings containing the data frame names
    :param ref_keys: a list of all the reference keys contained in data frame
    :param workspace_variables: the variables where all data are stored (defaults to global variables)

    """

    print(f'\n')
    print(f'Loading missing values for all columns of each data frame of the data frames '
          f'{data_frame_names}!')

    workspace_variables['total_types'] = set()
    for data_frame_name in trange(len(data_frame_names), file=stdout, leave=False, unit_scale=True,
                                  desc='finding missing values for all tables for all columns ...'):
        table_name = data_frame_names[data_frame_name]
        if "{}_cleaned".format(data_frame_name) in workspace_variables.keys():
            table_name = "{}_cleaned".format(data_frame_name)
        types = workspace_variables[table_name].dtypes
        types_iter_items = list(types.iteritems())
        for column_name_index in trange(len(types_iter_items), file=stdout, leave=False, unit_scale=True,
                                        desc='finding missing values for all columns of table {} ...'
                                             .format(table_name)):

            column_name = types_iter_items[column_name_index][0]
            ct = types_iter_items[column_name_index][1]
            if 'missing_values_{}'.format(table_name) not in workspace_variables:
                workspace_variables['missing_values_{}'.format(table_name)] = {}
            list_of_missing_values = \
                workspace_variables[table_name][workspace_variables[table_name][column_name].apply(
                    lambda value: ('object' == ct and (has_str_only_num(value, workspace_variables['total_types']) or
                                   is_num(value))) or is_empty_str_or_none(value, ct, workspace_variables['total_types']
                                  ) or check_pattern_of_date_column(value, column_name, '%Y-%m-%d %H:%M:%S.%f'))][
                    column_name].unique()
            workspace_variables['missing_values_{}'.format(table_name)][column_name] = list_of_missing_values
            # Add this line in order to check references
            check_if_column_name_contained_in_dict(ref_keys, column_name, data_frame_name)

    print(workspace_variables['total_types'])
    print(f'\n')
    print(f'Successfully calculated missing values for all columns of each data frame of the data frames '
          f'{data_frame_names}!')


def check_if_column_name_contained_in_dict(dict_of_ref_ids, column_name, data_frame_name,
                                           workspace_variables=globals()):
    """

    :param dict_of_ref_ids:
    :param column_name:
    :param data_frame_name:
    :param workspace_variables:

    """
    lst = []
    if data_frame_name in dict_of_ref_ids:
        for tup in dict_of_ref_ids[data_frame_name]:
            if tup[0] == column_name:
                lst = list(set(workspace_variables[tup[1]][tup[2]].values)
                           .difference(set(workspace_variables[data_frame_name][tup[0]])))
                break

    if len(lst) > 0:
        if workspace_variables['missing_values_{}'.format(data_frame_name)][column_name].size > 0:
            lst.extend(workspace_variables['missing_values_{}'.format(data_frame_name)][column_name].tolist())
        workspace_variables['missing_values_{}'.format(data_frame_name)][column_name] = array(lst)


def download_url(url, file_path_downloaded, where_to_unzip):
    """

    Downloads data in zip form and unzips it.

    If the file is not a zip one (this is checked after it is successfully downloaded within a file path name), an error
    is raised with appropriate message.

    If the url is not working properly, an exception is also raised with appropriate error

    :param url: the url from where to download the zip file
    :param file_path_downloaded: the path where we want the file to be stored as zip
    :param where_to_unzip: The path where we need to unzip the zip

    """

    path_to_store_var = Path('{}/{}'.format(file_path_downloaded, url.split('/')[-1]))
    path_to_store = str(path_to_store_var)

    print(f'\n')
    print(f'Downloading file {path_to_store} ...')

    tqdm(download_url_process(file_path_downloaded, path_to_store, url))

    print(f'\n')
    print(f'Downloading successfully file {path_to_store}')

    print(f'\n')
    print(f'Unzipping file {path_to_store} ...')

    unzip_file(path_to_store, where_to_unzip)

    print(f'\n')
    print(f'Unzipping file {path_to_store} was successful')


def unzip_file(path_to_store, where_to_unzip):
    """

    :param path_to_store:
    :param where_to_unzip:

    """
    with ZipFile(path_to_store, "r") as zip_ref:
        zip_ref.extractall(str(Path(where_to_unzip)))
        print(f'\n')
        print(f' file {where_to_unzip} was successfully unzipped.')


def download_url_process(file_path_downloaded, path_to_store, url):
    """

    Downloads specified url to the specified folder

    :param url: the url from where to download the zip file
    :param file_path_downloaded: the path where we want the file to be stored as zip
    :param path_to_store: The path where we need to store the zip

    """
    if Path(file_path_downloaded).is_dir():
        with get(url, stream=True) as r:
            r.raise_for_status()
            if not exists(path_to_store):
                with open(path_to_store, 'wb') as f:
                    for chunk in r.iter_content(chunk_size=8192):
                        # If you have chunk encoded response uncomment if
                        # and set chunk_size parameter to None.
                        # if chunk:
                        f.write(chunk)
    if not is_zipfile(path_to_store):
        raise Exception('File with name: {} is not of Zip format.'.format(path_to_store))


def load_data(my_path, workspace_variables=globals(), delimiter=','):
    """

    Loads data from a folder path name defined by param my_path.
    In case folder path name is not found, an empty list of files is returned.
    Thus, no data frames are loaded

    Pattern Name of each file within a folder name should be ^[a-zA-Z][a-zA-Z0-9]*_[0-9]+\.csv$
    or ^[a-zA-Z][a-zA-Z0-9]*$

    :param workspace_variables: the variables where all data are stored (defaults to global variables)
    :param my_path: the path folder, where to look for the files data to import (containing folders are ignored)
    :param delimiter: the delimiter used in order to split data
    :return: a list of all the string names of the data frames stored in workspace_variables

    """

    print(f'Loading data of folder: {my_path} to workspace memory ...')

    # 1. Get only the files contained in this folder
    files = [f for f in listdir(my_path) if isfile(join(my_path, f))]

    # 2. Construct a key value dictionary where key represents the name of data frame with which the respective
    #    file name starts. Value is a list of all the respective files from where data frame is being loaded
    list_common_names = {}
    for filename in trange(len(files), file=stdout, leave=False, unit_scale=True,
                           desc='Constructing dictionaries of filenames...'):
        filename_value = files[filename]
        if len(findall('[a-zA-Z][a-zA-Z0-9]*_[0-9]+\.csv', filename_value)) > 0:
            # CSV file name contains _ with number
            file_parts = filename_value.split('_')
        elif len(findall('[a-zA-Z][a-zA-Z0-9]*\.csv', filename_value)) > 0:
            # CSV file name is the name of the respective data frame to be stored
            file_parts = filename_value.split('.')
        else:
            continue
        if file_parts[0] in list_common_names.keys():
            list_common_names[file_parts[0]].append(filename_value)
        else:
            list_common_names[file_parts[0]] = [filename_value]

    # 3. we load for each key of the dictionary we constructed from above with the respective data frame in
    #    workspace_variables. If the list of the respective key within dictionary list_common_names contains
    #    more than one items we merge them within the same variable
    list_common_names_keys = list(list_common_names.keys())
    for key in trange(len(list_common_names_keys), file=stdout, leave=False, unit_scale=True,
                      desc='loading data sets...'):

        key_value = list_common_names_keys[key]
        if len(list_common_names[key_value]) > 1:
            data_frames = []
            for dfFile in list_common_names[key_value]:
                data_frames.append(read_csv("{}/{}".format(my_path, dfFile), delimiter))
            if key_value not in workspace_variables.keys():
                workspace_variables[key_value] = concat(data_frames)
        else:
            if key_value not in workspace_variables.keys():
                workspace_variables[key_value] = read_csv("{}/{}".format(my_path, list_common_names[key_value][0]),
                                                          delimiter)

    print(f'\n')
    print(f'Loading of data from folder {my_path} completed successfully!')

    return list(list_common_names.keys()).copy()


def encoding_vars_gaussian_or_conjugate_baysian(exception_dict, workspace_variables=globals()):
    """

    :param exception_dict:
    :param workspace_variables:
    "param exception_dict:
    :return:

    """

    print(f'\n')
    print(f'Applying encoding for all columns of each data frame of the data frames '
          f'{workspace_variables["all_keys"]}!')

    dict_table_columns_for_conjugate_baysian = {}
    list_all_keys = list(workspace_variables["all_keys"])
    for data_frame_name_ind in trange(len(list_all_keys), file=stdout, leave=False,
                                      unit_scale=True, desc='encoding of all data sets columns ...'):

        data_frame_name = list_all_keys[data_frame_name_ind]
        types = workspace_variables['{}_cleaned'.format(data_frame_name)].dtypes
        workspace_variables['unique_values_of_{}_cleaned'.format(data_frame_name)] = {}
        workspace_variables['value_counts_of_{}_cleaned'.format(data_frame_name)] = {}
        workspace_variables['{}_encoded'.format(data_frame_name)] = DataFrame()
        types_items = list(types.items())
        if 'mappings_of_{}_categorically_encoded_columns'.format(data_frame_name) not in workspace_variables:
            workspace_variables['mappings_of_{}_categorically_encoded_columns'.format(data_frame_name)] = {}
        for column_name_ind in trange(len(types_items), file=stdout, leave=False, unit_scale=True,
                                      desc='encoding of all columns of data set {}...'.format(data_frame_name)):

            column_name = types_items[column_name_ind][0]
            obj_type = types_items[column_name_ind][1]
            workspace_variables['unique_values_of_{}_cleaned'.format(data_frame_name)][column_name] = \
                workspace_variables['{}_cleaned'.format(data_frame_name)][column_name].unique()
            workspace_variables['value_counts_of_{}_cleaned'.format(data_frame_name)][column_name] = \
                workspace_variables['{}_cleaned'.format(data_frame_name)][column_name].value_counts()
            if (obj_type == float or str(obj_type) == 'float64' or obj_type == int or
                str(obj_type) == 'int64' or obj_type == complex) and (data_frame_name not in exception_dict.keys() or
                                                                      column_name not in exception_dict[data_frame_name]
            ) and 'date' not in column_name.lower():
                unique_values = \
                    workspace_variables['unique_values_of_{}_cleaned'.format(data_frame_name)][column_name]
                workspace_variables['{}_encoded'.format(data_frame_name)][column_name] = \
                    workspace_variables['{}_cleaned'.format(data_frame_name)][column_name].copy(deep=True).map(
                        dict(zip(unique_values, QuantileTransformer(len(unique_values),
                                                                    output_distribution='normal').fit_transform(
                            [[element] for element in list(unique_values)]
                        )))).apply(lambda x: x[0])

                workspace_variables['mappings_of_{}_categorically_encoded_columns'.format(data_frame_name)]\
                    [column_name] = concat([workspace_variables['{}_cleaned'.format(data_frame_name)][column_name],
                                            workspace_variables['{}_encoded'.format(data_frame_name)][column_name]],
                                            axis=1, names=['raw_{}'.format(column_name), 'mapped_{}'
                                                   .format(column_name)]).copy(deep=True).drop_duplicates()

            elif 'date' in column_name.lower():
                workspace_variables['{}_encoded'.format(data_frame_name)][column_name] = \
                    workspace_variables['{}_cleaned'.format(data_frame_name)][column_name] \
                        .apply(lambda x: round(mktime(datetime.strptime(x, "%Y-%m-%d %H:%M:%S.%f").timetuple())))

            else:
                if data_frame_name not in exception_dict or column_name not in exception_dict[data_frame_name]:
                    if data_frame_name in dict_table_columns_for_conjugate_baysian:
                        dict_table_columns_for_conjugate_baysian[data_frame_name].append(column_name)
                    else:
                        dict_table_columns_for_conjugate_baysian[data_frame_name] = [column_name]

    encode_conjugate_baysian_and_standardize_data_frames(dict_table_columns_for_conjugate_baysian, workspace_variables)

    move_from_exception_dict_to_encoded(exception_dict, workspace_variables)

    print(f'\n')
    print(f'Applying encoding for all columns of each data frame of the data frames'
          f'{workspace_variables["all_keys"]}!')


def encode_conjugate_baysian_and_standardize_data_frames(dict_table_columns_for_conjugate_baysian,
                                                         workspace_variables=globals()):
    """

    :param dict_table_columns_for_conjugate_baysian:
    :param workspace_variables:

    """
    dict_table_columns_for_conjougate_baysian_items = list(dict_table_columns_for_conjugate_baysian.items())
    for key_index in trange(len(dict_table_columns_for_conjougate_baysian_items), file=stdout, leave=False,
                            unit_scale=True,
                            desc='encoding categorical variables...'):

        key = dict_table_columns_for_conjougate_baysian_items[key_index][0]
        columns = dict_table_columns_for_conjougate_baysian_items[key_index][1]
        categorical_columns_encoded = '{}_categorical_columns_encoded'.format(key)
        counter = 1
        while categorical_columns_encoded in workspace_variables['{}_cleaned'.format(key)]:
            categorical_columns_encoded = '{}_categorical_columns_encoded_{}'.format(key, counter)
            counter = counter + 1
        workspace_variables['{}_encoded'.format(key)] \
            .insert(workspace_variables['{}_encoded'.format(key)].shape[1],
                    categorical_columns_encoded,
                    [x for x in range(0, len(workspace_variables['{}_cleaned'.format(key)].index))])
        enc = GIGEncoder()
        enc.fit(workspace_variables['{}_cleaned'.format(key)][columns].copy(deep=True),
                workspace_variables['{}_encoded'.format(key)][categorical_columns_encoded])
        # print(enc._gig_distributions)
        workspace_variables['{}_encoded'.format(key)][categorical_columns_encoded] = \
            enc.transform(workspace_variables['{}_cleaned'.format(key)][columns].copy(deep=True), columns=columns)

        workspace_variables['mappings_of_{}_categorically_encoded_columns'.format(key)][categorical_columns_encoded] =\
            concat([workspace_variables['{}_cleaned'.format(key)][columns].copy(deep=True),
                    workspace_variables['{}_encoded'.format(key)][categorical_columns_encoded]], axis=1)\
                                                    .copy(deep=True).drop_duplicates()

    get_all_unique_values(workspace_variables['all_keys'], '{}_encoded', 'unique_values_of_{}',
                                                           'value_counts_of_{}', workspace_variables)

    scheme = scheme_to_dict(workspace_variables['all_keys'], '{}_encoded', workspace_variables)

    fill_na_with_freq_or_unknown(scheme, 0, '{}_encoded', 'value_counts_of_{}', workspace_variables)


def scheme_to_dict(data_frame_names, pattern_data_frame, workspace_variables=globals()):
    """

    :param data_frame_names:
    :param pattern_data_frame:
    :param workspace_variables:

    """
    workspace_variables[pattern_data_frame.format('dict_all_columns')] = {}
    for data_frame_name in trange(len(data_frame_names), file=stdout, leave=False, unit_scale=True,
                                  desc='converting data frame names to scheme...'):
        df_name = data_frame_names[data_frame_name]
        workspace_variables[pattern_data_frame.format('dict_all_columns')][pattern_data_frame.format(df_name)] =\
            workspace_variables[pattern_data_frame.format(df_name)].dtypes.index.tolist()

    return workspace_variables[pattern_data_frame.format('dict_all_columns')]


def move_from_exception_dict_to_encoded(exception_dict, workspace_variables=globals()):
    """

    :param exception_dict:
    :param workspace_variables:

    """
    exception_dict_items = list(exception_dict.items())
    for key_index in trange(len(exception_dict_items), file=stdout, leave=False, unit_scale=True,
                            desc='Moving from cleaned to encoded without encoding variables ...'):
        key = exception_dict_items[key_index][0]
        columns = exception_dict_items[key_index][1]
        for column_index in trange(len(columns), file=stdout, leave=False, unit_scale=True,
                                   desc='Moving from cleaned to encoded without encoding, the variable {} ...'
                                        .format(key)):
            column = columns[column_index]
            workspace_variables['{}_encoded'.format(key)] \
                .insert(workspace_variables['{}_encoded'.format(key)].shape[1],
                        column, workspace_variables['{}_cleaned'.format(key)][column])


def remove_columns_from_tables(dict_to_be_removed_columns, workspace_variables=globals()):
    """

    :param dict_to_be_removed_columns:
    :param workspace_variables:

    """
    dict_to_be_removed_columns_items = list(dict_to_be_removed_columns.items())
    for table_index in trange(len(dict_to_be_removed_columns_items), file=stdout, leave=False,
                              unit_scale=True, desc='Removing columns indicated in dictionary ...'):

        table = dict_to_be_removed_columns_items[table_index][0]
        columns_to_remove = dict_to_be_removed_columns_items[table_index][1]
        if len(columns_to_remove) == workspace_variables[table].shape[1]:
            print('[]    []     []'.format(columns_to_remove, workspace_variables[table].shape[1], table))
            workspace_variables['all_keys'].remove(table)
        else:
            workspace_variables["{}_cleaned".format(table)] = \
                workspace_variables[table].copy(deep=True).drop(columns_to_remove, axis=1)


def replace_all_missing_with_na(workspace_variables=globals()):
    """

    :param workspace_variables:
    :return:

    """
    list_all_keys = list(workspace_variables['all_keys'])
    for table_index in trange(len(list_all_keys), file=stdout, leave=False,
                              unit_scale=True, desc='Replacing all missing values with na for all '
                                                    'columns in all data sets ...'):
        table = list_all_keys[table_index]
        table_name = table
        if "{}_cleaned".format(table) in workspace_variables.keys():
            table_name = "{}_cleaned".format(table)
        types = workspace_variables[table_name].dtypes
        dict_values_columns_to_replace = {}
        types_keys = list(types.keys())
        for column_name_index in trange(len(types_keys), file=stdout, leave=False, unit_scale=True,
                                        desc='Replacing all missing values with na for all columns in data set {} ...'
                                             .format(table_name)):

            column_name = types_keys[column_name_index]
            dict_values_columns_to_replace[column_name] = {}
            for missing_value in workspace_variables['missing_values_{}'.format(table)][column_name]:
                dict_values_columns_to_replace[column_name][missing_value] = nan

        workspace_variables[table_name] = \
            workspace_variables[table_name].replace(dict_values_columns_to_replace)


def fill_na_with_freq_or_unknown(dict_to_be_cleaned_columns, is_fill_na_freq_or_unknown_max, pattern_of_table,
                                 pattern_of_unique_values_table, workspace_variables=globals()):
    """

    :param is_fill_na_freq_or_unknown_max:
    :param dict_to_be_cleaned_columns:
    :param pattern_of_table
    :param pattern_of_unique_values_table

    """

    dict_to_be_cleaned_columns_items = list(dict_to_be_cleaned_columns.items())
    for table_index in trange(len(dict_to_be_cleaned_columns_items), file=stdout, leave=False,
                              unit_scale=True, desc='Filling all na values with most frequent value in all'
                                                    ' columns of all data sets ...'):

        table = dict_to_be_cleaned_columns_items[table_index][0]
        columns_to_fill = dict_to_be_cleaned_columns_items[table_index][1]
        table_name = table
        if pattern_of_table.format(table) in workspace_variables.keys():
            table_name = pattern_of_table.format(table)
        types = workspace_variables[table_name].dtypes
        dict_values_columns_to_fill = {}
        types_keys = list(types.keys())
        for column_name_index in trange(len(types_keys), file=stdout, leave=False, unit_scale=True,
                                        desc='Filling all na values with most frequent value in all columns of'
                                             ' data set {} ...'.format(table_name)):

            column_name = types_keys[column_name_index]
            if is_fill_na_freq_or_unknown_max == 0:
                if column_name in columns_to_fill:
                    dict_values_columns_to_fill[column_name] = \
                        workspace_variables[pattern_of_unique_values_table.format(table)][column_name].idxmax()
            else:
                if column_name in columns_to_fill:
                    dict_values_columns_to_fill[column_name] = "UNKNOWN"

        workspace_variables[table_name] = \
            workspace_variables[table_name].fillna(value=dict_values_columns_to_fill)


def main(workspace_variables):
    """

    :param workspace_variables:
    """
    # 0 Inputs
    get_ipython().magic("%reset -f")

    url = "https://bitbucket.org/spirob87/data_mining_course_2020/raw/ce0a2726d8426240e64aa633655ffeb3758f20bc" \
          "/project_data.zip"
    base_path = Path('project_data')
    file_name_down = "project_data"
    # a dictionary containing  the data frames which contain a foreign key to another data table each of which is
    # mapped to a list of 3-dimensional tuples. Each tuple contains in this specific order the name of the ref id,
    # the pattern it matches, the data frame to which it refers and the specific column name to which it refers
    reference_keys = {'devices': [('user_id', 'users', 'user_id')],
                      'transactions': [('user_id', 'users', 'user_id')],
                      'notifications': [('user_id', 'users', 'user_id')]}
    dict_to_be_removed_columns = {
        'transactions': ['ea_merchant_city'],
        'users': ['city']
    }
    dict_fill_na_unknown = {
        'devices': ['brand'],
        'transactions': ['ea_cardholderpresence']
    }
    dict_fill_na_freq_data_col = {'transactions': ['ea_merchant_country', 'ea_merchant_mcc', 'ea_cardholderpresence'],
                                  'users': ['attributes_notifications_marketing_email',
                                            'attributes_notifications_marketing_push']}
    exception_dict_to_do_conjugate_baysian_encoding = {'users': ['age']}
    # dict_fill_na_unknown = {'users': 'city'}

    plot_variables = {
        'users': ['age', 'users_categorical_columns_encoded'],
        'transactions': ['amount_usd', 'transactional_categorical_columns_encoded'],
        'notifications': ['notifications_categorical_columns_encoded']
    }

    if not exists('project_data'):
        makedirs('project_data')
    if not exists('temp_data'):
        makedirs('temp_data')

    # 1. download the zip file and load the data
    # tqdm(download_url(url, file_name_down, base_path))
    workspace_variables['all_keys'] = load_data(base_path, workspace_variables)
    print(workspace_variables['all_keys'])

    # 1.2 Task. Find the missing / wrong values of data (2nd bullet of task 1)
    get_all_unique_values(workspace_variables['all_keys'], '{}', 'unique_values_of_{}', 'value_counts_of_{}',
                          workspace_variables)
    get_missing_values(workspace_variables['all_keys'], reference_keys, workspace_variables)

    # TODO descriptive statistics

    # 1.4 Task. Obtain insight through descriptive statistics (4th bullet of task 1)
    get_descriptive_statistics(workspace_variables['all_keys'], workspace_variables)

    # 2.2 Task. Clean data (2th bullet of task 2)
    clean_data(dict_fill_na_freq_data_col, dict_to_be_removed_columns, dict_fill_na_unknown, workspace_variables)

    # 3.1 part (a) Task metric. Clean data (1st bullet of task 3)
    calculate_and_check_unengaged_users(workspace_variables)

    # 2.3 Task. Encode data (3rd bullet of task 2)
    encode_variables(exception_dict_to_do_conjugate_baysian_encoding, workspace_variables)

    print(workspace_variables['users_encoded'].isnull().sum())

    # 1.3 Task & 2.2 Task. Finding Outliers using z-score method and cleaning outliers
    # Calculate outliers after encoding with z-score technique and visualize in the form of Histograms
    outliers_z_score_visual = OutliersZScoreVisual(plot_variables, workspace_variables=workspace_variables)
    outliers_z_score_visual.z_score_encoded(workspace_variables['all_keys'])

    # 2.1 Task. Combine Variables
    # Combine all variables to one table in order to perform ML algorithm
    combine_variables(workspace_variables)

    # 3.1 part (b) Task metric. Clean data (1st bullet of task 3)
    calculate_unengaged_users_metric_part_by_most_recent_transaction(workspace_variables)

    # Prepare table before modeling
    prepare_for_modeling(workspace_variables)

    # 2.4 KPI
    create_kpi(workspace_variables)


def prepare_for_modeling(workspace_variables=globals()):

    max_created_date_user = workspace_variables['finalized_data']['created_date_user'].max()
    ind = range(1, len(workspace_variables['unengaged_users_not_recent']) + 1)
    a = Series(list(workspace_variables['unengaged_users_not_recent']), ind)
    a.rename('crated_date_user')
    b = Series([max_created_date_user for x in ind], ind)
    b.rename('most_recent_transaction')
    workspace_variables['new_data_frame_model'] = concat([a, b], axis=1).drop_duplicates()
    workspace_variables['new_data_frame_model'].columns = ['created_date_user', 'most_recent_transaction']
    workspace_variables['most_recent_transaction_per_user_id'] = concat([
        workspace_variables['most_recent_transaction_per_user_id'], workspace_variables['new_data_frame_model']])

    workspace_variables['finalized_data'] = workspace_variables['finalized_data']\
        .merge(workspace_variables['most_recent_transaction_per_user_id'], left_on='created_date_user',
               right_on='created_date_user', how='outer')


def init_cleaned_data_frames_as_orig_copies(data_frame_names, workspace_variables=globals()):
    """

    :param data_frame_names:
    :param workspace_variables:

    """
    for table_index in trange(len(data_frame_names), file=stdout, leave=False,
                              unit_scale=True, desc='Initializing cleaned data frames ...'):
        table = data_frame_names[table_index]
        workspace_variables['{}_cleaned'.format(table)] = workspace_variables[table].copy(deep=True)
    print('Successfully initialized data frames')


def clean_data(dict_fill_na_freq_data_col, dict_to_be_removed_columns, dict_fill_na_unknown,
               workspace_variables=globals()):
    """

    :param dict_fill_na_freq_data_col:
    :param dict_to_be_removed_columns:
    :param dict_fill_na_unknown:
    :param workspace_variables:

    """

    # Initialize cleaned data based on original copies
    init_cleaned_data_frames_as_orig_copies(workspace_variables['all_keys'], workspace_variables)
    # Remove columns from columns of tables of the respective dictionary
    remove_columns_from_tables(dict_to_be_removed_columns, workspace_variables=workspace_variables)
    # Replace all missing values with na
    replace_all_missing_with_na(workspace_variables)
    # Fill all missing values of the respective dictionary with most frequent data from the column
    fill_na_with_freq_or_unknown(dict_fill_na_freq_data_col, 0, '{}_cleaned', 'value_counts_of_{}', workspace_variables)
    # Fill all missing values of the respective dictionary with "UNKNOWN"
    fill_na_with_freq_or_unknown(dict_fill_na_unknown, 1, '{}_cleaned', 'value_counts_of_{}', workspace_variables)

    tqdm(manual_clean(workspace_variables))


def manual_clean(workspace_variables):
    """

    :param workspace_variables:

    """
    # Replace user id with creation user date in transactions
    s = workspace_variables['transactions_cleaned']['user_id'] \
        .map(workspace_variables['users_cleaned'].set_index('user_id')['created_date'])
    df = workspace_variables['transactions_cleaned'].drop('user_id', 1).assign(created_date_user=s)
    workspace_variables['transactions_cleaned'] = df.copy(deep=True)
    # Rename column of Users named 'created_date' to 'created_date_user'
    workspace_variables['users_cleaned'] = workspace_variables['users_cleaned'].rename(
        columns={"created_date": "created_date_user"}).copy(deep=True)
    # Drop unused column user id in users (identifier is creation date of user from here on, since it uniquely
    # identifies user)
    workspace_variables['users_cleaned'] = workspace_variables['users_cleaned'].drop('user_id', 1)
    # Calculate age of user from column birth_year and rename column birth_year to age
    workspace_variables['users_cleaned']['birth_year'] = workspace_variables['users_cleaned']['birth_year'] \
        .apply(lambda year: datetime.now().year - int(year))
    workspace_variables['users_cleaned'] = workspace_variables['users_cleaned'].rename(columns={"birth_year": "age"})
    # Calculate all values in negative form for outbound transactions.
    workspace_variables['transactions_cleaned']['amount_usd'] = \
        where(workspace_variables['transactions_cleaned']['direction'] != 'INBOUND',
              -1 * workspace_variables['transactions_cleaned']['amount_usd'],
              workspace_variables['transactions_cleaned']['amount_usd'])
    # Remove unused column direction
    workspace_variables["transactions_cleaned"] = workspace_variables["transactions_cleaned"].drop(['direction'],
                                                                                                   axis=1)


def combine_variables(workspace_variables=globals()):
    list_all_keys = list(workspace_variables['all_keys'])
    for key in trange(len(list_all_keys), file=stdout, leave=False,
                      unit_scale=True, desc='Combining all columns to 1 data frame, for each data frame name ...'):
        key_value = list_all_keys[key]
        workspace_variables['{}_encoded_without_z_outliers_all_cols'.format(key_value)] =\
            concat(list(workspace_variables['{}_encoded_without_z_outliers'.format(key_value)]
                        .values()), axis=1).reset_index().copy(deep=True)
        del workspace_variables['{}_encoded_without_z_outliers'.format(key_value)]
        workspace_variables['{}_encoded_without_z_outliers'.format(key_value)] =\
            workspace_variables['{}_encoded_without_z_outliers_all_cols'.format(key_value)].copy(deep=True)
        del workspace_variables['{}_encoded_without_z_outliers_all_cols'.format(key_value)]

    workspace_variables['finalized_data'] = merge(
        workspace_variables['users_encoded_without_z_outliers'],
        workspace_variables['transactions_encoded_without_z_outliers'], how='outer', on='created_date_user')
    workspace_variables['finalized_data'] = workspace_variables['finalized_data'].drop(['index_x', 'index_y'], axis=1)


def encode_variables(exception_dict, workspace_variables=globals()):
    """

    :param exception_dict:
    :param workspace_variables:

    """

    encoding_vars_gaussian_or_conjugate_baysian(exception_dict, workspace_variables)

    tqdm(sort_all_data_sets(workspace_variables))


def sort_all_data_sets(workspace_variables=globals()):
    """

    :param workspace_variables:

    """
    workspace_variables['transactions_cleaned'] = workspace_variables['transactions_cleaned'] \
        .sort_values(['created_date', 'created_date_user', 'amount_usd']) \
        .reset_index(drop=True)
    workspace_variables['users_cleaned'] = workspace_variables['users_cleaned'] \
        .sort_values(['created_date_user', 'age']).reset_index(drop=True)
    workspace_variables['transactions_encoded'] = workspace_variables['transactions_encoded'] \
        .sort_values(['created_date', 'created_date_user', 'amount_usd']) \
        .reset_index(drop=True)
    workspace_variables['users_encoded'] = workspace_variables['users_encoded'] \
        .sort_values(['created_date_user', 'age']).reset_index(drop=True)


def calculate_unengaged_users_metric_part_by_most_recent_transaction(workspace_variables=globals()):
    most_recent_transaction_per_user_id = workspace_variables['finalized_data'].groupby('created_date_user') \
        ['created_date'].max().to_frame('most_recent_transaction').reset_index()
    mx = most_recent_transaction_per_user_id

    # mx['most_recent_transaction'] = mx['most_recent_transaction'].astype('datetime64')  # .astype(int).astype(float)

    #    one   two
    # a   1   2014-07-09
    # b   2   2014-07-10
    # c   3   2014-07-11
    # Then calculate the mean of column "two" by:
    # (df.two - df.two.min()).mean() + df.two.min()

    mean = (mx['most_recent_transaction'] - mx['most_recent_transaction'].min()).mean() + mx[
        'most_recent_transaction'].min()
    print('most_recent_transaction Min:', mx['most_recent_transaction'].min())
    print('most_recent_transaction Min:', mx['most_recent_transaction'].max())
    print(mean)

    workspace_variables['most_recent_transaction_per_user_id'] = most_recent_transaction_per_user_id
    churned = most_recent_transaction_per_user_id[most_recent_transaction_per_user_id['most_recent_transaction'] < mean]
    active = most_recent_transaction_per_user_id[most_recent_transaction_per_user_id['most_recent_transaction'] >= mean]

    print("Active Users: ", active.count())
    print("Churned Users: ", churned.count())

    workspace_variables['unengaged_users_not_recent'] =\
        most_recent_transaction_per_user_id[most_recent_transaction_per_user_id['most_recent_transaction']
                                        < mean]['created_date_user'].unique().tolist()

    workspace_variables['unengaged_users_total'] = set(workspace_variables['unengaged_users']\
        + workspace_variables['unengaged_users_not_recent'])


def create_kpi(workspace_variables=globals()):
    workspace_variables['KPI1'] = workspace_variables['transactions_cleaned'].copy(deep=True)
    workspace_variables['KPI1']['date_of_user_creation'] = workspace_variables['KPI1']['created_date_user']
    workspace_variables['KPI1'] = workspace_variables['KPI1'].groupby('date_of_user_creation')\
        .agg({'created_date_user': 'count', 'amount_usd': 'sum'})
    workspace_variables['KPI1'].rename(columns={'created_date_user': 'number_of_transactions'}, inplace=True)
    workspace_variables['KPI1']['date'] = workspace_variables['KPI1'].index.to_series(name='temp')
    workspace_variables['KPI1'].reset_index(drop=True, inplace=True)
    workspace_variables['KPI1'].rename(columns={'date': 'created_date_user'}, inplace=True)


def calculate_unengaged_users_metric_part_before_encoding(workspace_variables=globals()):
    """

    :param workspace_variables:

    """

    workspace_variables['unengaged_users'] = \
        list(set(workspace_variables['users_cleaned']['created_date_user'].unique().tolist()).symmetric_difference(
        set(workspace_variables['transactions_cleaned']['created_date_user'].unique().tolist())))

    workspace_variables['unengaged_users'] = \
        list(map(lambda x: round(mktime(datetime.strptime(x, "%Y-%m-%d %H:%M:%S.%f").timetuple())),
            workspace_variables['unengaged_users']))
    workspace_variables['users_cleaned'].drop(workspace_variables['users_cleaned']\
        [workspace_variables['users_cleaned']
            .apply(lambda x: round(mktime(datetime.strptime(x['created_date_user'], "%Y-%m-%d %H:%M:%S.%f").timetuple()))\
                             in workspace_variables['unengaged_users'], axis=1)].index, inplace=True)

    print(f'\n')
    print('Successfully loaded the unengaged users.')


def calculate_and_check_unengaged_users(workspace_variables=globals()):
    # 4. Compute all values of reference keys that we do not need (unneeded values)
    print(f'\n')
    print('Loading unengaged users ... ')
    return tqdm(calculate_unengaged_users_metric_part_before_encoding(workspace_variables))


if __name__ == '__main__':
    """
    """
    tqdm(main(globals()))
