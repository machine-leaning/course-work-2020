#!/usr/bin/env python3
"""

By executing this file you install converter from py to jypiter notebook

"""
# Importing here IPython does nothing specific. It is present just for compilation reasons within PyCharm
#       (for whoever runs Pycharm)
from IPython import get_ipython
from os import system
from pathlib import Path

get_ipython().magic("%cd {}".format(Path.home()))
system("git clone https://github.com/williamjameshandley/py2nb")
get_ipython().magic("%cd py2nb")
get_ipython().magic("%run setup.py install")
