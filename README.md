## TASKS

1. Exploratory Data Analysis  [GROUP]

- [ ] 1.1 Understand what each table/column represents
- [ ] 1.2 Recognize if there are missing/wrong values in the data
- [ ] 1.3 Check for outliers in the data
- [ ] 1.4 Obtain insights through descriptive statistics
- [ ] 1.5 Identify key relationships within the data

2. Initial preprocessing  [GROUP]

- [ ] 2.1 Combine whatever information you need from the different tables
- [ ] 2.2 Clean the data (handling missing values, fixing errors, outliers, etc.)
- [ ] 2.3 Encode the variables
- [ ] 2.4 Perform any feature engineering step you see fit (group features, define KPIs, etc.)

3. Identify Unengaged and Churned Users  [GROUP]

- [ ] 3.1 Define a target metric to measure user engagement.How would you define an ​engaged vs. unengaged ​user?
- [ ] 3.2 Using your logic from above, build a model (heuristic/statistical/ML)to classify ​engaged and unengaged ​users.

4. Actionable decisions (critical discussion)  [INDIVIDUAL]

- [ ] 4.1 How many users are classified as churn with your method?
- [ ] 4.2 How would you setup a test/experiment to check whether we are actually reducing churn?
- [ ] 4.3 What metrics and techniques would you use to assess the impact of the business action?

Report [INDIVIDUAL]

- [ ] The report should be split in 4 parts, one for each part of the assessment
- [ ] You should discuss the techniques you followed (answering why you choose a specific technique over another) and the outcomes
- [ ] Also, discuss what you would have done differently given more time. What else would you have tried?

## REFERENCES & RESOURCES
---

## Country/Currency	Acronym/AbbreviationAcr./Abb.

United Arab Emirates Dirham | AED
---                         | ---
Sweden Krona	            | SEK
Australia Dollar            | AUD
Great Britain Pound	        | GBP
Ethereum cryptocurrency     | ETH
Russia Rouble	            | RUB
Switzerland Franc	        | CHF
Croatia Kuna	            | HRK
Litecoin is a peer-to-peer cryptocurrency | (LTC or Ł)
Morocco Dirham	            | MAD
Bitcoin                     | BTC
New Zealand Dollar	        | NZD
Japan Yen	                | JPY
Israel New Shekel	        | ILS
Qatar Rial	                | QAR
Mexico Peso	                | MXN
Denmark Krone	            | DKK
Singapore Dollar	        | SGD
South Africa Rand	        | ZAR
Bulgaria Lev	            | BGN
USA Dollar	                | USD
India Rupee	                | INR
Thailand Baht	            | THB
Romania New Lei	            | RON
Hungary Forint	            | HUF
Turkish New Lira	        | TRY
Ripple cryptocurrency       | XRP
Poland Zloty	            | PLN
Euro	                    | EUR
Bitcoin Cash cryptocurrency | BCH
Czech Koruna	            | CZK
Canada Dollar	            | CAD
Norway Kroner	            | NOK
Hong Kong Dollar	        | HKD
Saudi Arabia Riyal	        | SAR


## TASK 1.1
Understand what each table/column represents.

USERS DF
--------
1. index: the unique index of each user
2. user_id: the name of each user in number format
3. birth_year: the date each user was born
4. country: the country where each user is located
5. city: the city where each user is located
6. created_date: the date when each user was signed up to the bank app
7. user_settings_crypto_unlocked: the date when cryptocurrency unlocked(aka who has cryptocurrency)
8. plan: user's cell phone plan 
9. attributes_notifications_marketing_push: which users have selected push notifications for marketing purposes
10. attributes_notifications_marketing_email: which users have selected email notifications for marketing purposes
11. num_contacts: the number of contacts each of them have
12. num_referrals: the number of people the users have give a referral about the app
13. num_successful_referrals: the number of people successfully signed up after the referral

DEVICES DF
----------
1. index: the unique index of each user
2. brand: mobile os
3. user_id: the name of each user in number format

NOTIFICATION DF
---------------

1. index: the unique index of each user
2. reason: the purpose of the notification
3.channel: in what way the notification was sent
4. status: the status of the notification (whether the notification was sent or not)
5. user_id: the name of each user in number format
6. created_date: when the notification was sent


TRANSACTIONS DF
---------------

1. transaction_id: the unique number of each transaction
2. transactions_type: the type of transaction(transfer, withdrawal, deposits, etc.)
3. transactions_currency: the currency of each transaction
4. amount_usd: the amount of money transferred in US dollars
5. transactions_state: the state of the transaction
6. ea_cardholder_presence: (?)
7. ea_merchant_mcc: Merchant Category Codes (MCC)
8. ea_merchant_city: the city where the merchant is located 
9. ea.merchant_country: the country where the  merchant is located  
1O. direction: type of transaction
11. user_id: the name of each user in number format
12. created_date: the date the transaction taken place
